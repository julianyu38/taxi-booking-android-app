package com.icanstudioz.taxicustomer.session;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.icanstudioz.taxicustomer.acitivities.LoginActivity;
import com.icanstudioz.taxicustomer.pojo.User;

import java.util.HashMap;


/**
 * Created by android on 9/3/17.
 */


public class SessionManager {


    static SessionManager app;
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "taxiapp";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "mobile";
    public static final String AVATAR = "avatar";
    public static final String GCM_TOKEN = "gcm_token";


    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";
    public static final String FARE_UNIT = "unit";
    public static final String COST = "cost";
    public static final String LOGIN_AS = "login_as";
    public static final String USER_ID = "user_id";
    public static final String IS_ONLINE = "false";
    public String KEY = "key";
    public static final String USER = "user";
    private String avatar;


    private SessionManager() {
    }


    public static SessionManager getInstance() {
        if (app == null) {
            app = new SessionManager();

        }
        return (app);
    }

    public SharedPreferences setPref(Context context) {

        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        return pref;

    }

    public String getName() {
        return getUser().getName();
    }

    public void setKEY(String k) {
        editor.putString(KEY, k);
        editor.commit();
    }

    public void setGcmToken(String gcmToken) {
        editor.putString(GCM_TOKEN, gcmToken);
        editor.commit();
    }

    public void setUnit(String unit) {
        editor.putString(FARE_UNIT, unit);
        editor.commit();


    }public void setCost(String cost) {
        editor.putString(COST, cost);
        editor.commit();


    }

    public String getUnit() {
      return   pref.getString(FARE_UNIT, null);
    }

    public String getGcmToken() {
       return pref.getString(GCM_TOKEN, null);
    }


    public void setStatus(String staus) {
        editor.putString(IS_ONLINE, staus);
        editor.commit();
    }

    public String getStatus() {

        return pref.getString(IS_ONLINE, null);
    }

    public String getKEY() {
        return getUser().getKey();

    }

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void setIsLogin() {
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String name, String email, String user, String avatar, String mobile) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_MOBILE, mobile);
        // Storing email in pref
        editor.putString(KEY_EMAIL, email);
        editor.putString(USER_ID, user);
        editor.putString(AVATAR, avatar);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil setting user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(AVATAR, pref.getString(AVATAR, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        /*i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick setting for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getAvatar() {
        return getUser().getAvatar();
    }

    public String getUid() {
        return getUser().getUser_id();
    }

    public void setUser(String user) {
        editor.putString(USER, user);
        editor.commit();
    }

    public User getUser() {
        Gson gson = new Gson();
        return gson.fromJson(pref.getString(USER, null), User.class);

    }

    public void setAvatar(String avatar) {
        editor.putString(AVATAR, avatar);
        editor.commit();
    }
}
